# This is homework project for implementing a universal Turing machine.

## Building
You'll need to have cmake >= 3.5 installed and added to your path.
Open your terminal in root of this repository and run the following code:

```bash
mkdir build
cd build
cmake ..
```
Built project files will be placed in "build" folder.
Note: If you are on macOS you might want to run cmake .. -G"Xcode" to generate Xcode project.  

### Remember - building in release mode will yield the best performance.

## Entities

### turing
This executable allows reading files with initial tape data and commands, as well as performing a given number of steps.  
Usage: ```./turing -f <input_file_name> -n <number of steps to perform>```  
Call ```turing -h``` to get more details.