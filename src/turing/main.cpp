#include <iostream>
#include <map>
#include <fstream>
#include <string>
#include <stdlib.h>


using namespace std;

struct Transition {
    string current_state;
    char current_symbol;
    char new_symbol;
    char dir;
    string new_state;
    
    friend istream& operator>>(istream& in, Transition& tr)
    {
        return in >> tr.current_state >> tr.current_symbol >> tr.new_symbol >> tr.dir >> tr.new_state;
    }
};

class TuringMachine {
    
public:
    TuringMachine(int32_t position, string tape) : current_pos(position), cur_state("0"), dir('R'), halted(false)
    {
        machine_tape = tape;
        
        if(position < 0 || position >= tape.length())
            throw string("The starting position is out of range!!!");
    }
    
    bool has_halted() const
    {
        return halted;
    }
    
    const string& get_current_state() const
    {
        return cur_state;
    }
    
    int32_t get_current_pos() const
    {
        return current_pos;
    }
    
    char get_direction() const
    {
        return dir;
    }
    
    const string& get_tape() const
    {
        return machine_tape;
    }
    
    void add_state_transition(Transition transition)
    {
        transitions[transition.current_state][transition.current_symbol] = transition;
    }
    
    void advance_state()
    {
        if (halted)
            throw string("The machine has already halted, but you are still trying to advance it!!!");
        
        char current_symbol = machine_tape[current_pos];
        
        map<string, map<char, Transition> >::iterator state_transitions_it;
        
        //Find all the transitions from the current state
        state_transitions_it = transitions.find(cur_state);
        
        if (state_transitions_it != transitions.end())
        {
            map<char, Transition> command_map = state_transitions_it->second;
            
            //Find the transition from the current state with the current character
            map<char, Transition>::iterator command_it = command_map.find(current_symbol);
            
            if (command_it != command_map.end())
            {
                Transition tr = command_it->second;
                
                machine_tape[current_pos] = tr.new_symbol;
                cur_state = tr.new_state;
                
                dir = tr.dir;
                
                if (dir == 'L' || dir == 'l')
                {
                    current_pos--;
                }
                else
                    current_pos++;
                
                //Prevent out of bounds reads
                if (current_pos < 0 || current_pos >= machine_tape.length())
                    halted = true;
                
                //Additional check to halt on transition to state that begins with halt
                size_t substring_idex = cur_state.find("halt");
                if(substring_idex == 0)
                {
                    halted = true;
                }
            }
            else
            {
                //No transition from this state with this symbol
                halted = true;
                return;
            }
        }
        else
        {
            //No transitions from this state, halt
            halted = true;
            return;
        }
    }
    
private:
    string machine_tape;
    int32_t current_pos;
    string cur_state;
    char dir;
    
    bool halted;
    map<string, map<char, Transition> > transitions;
};

class Parameters{
public:
    
    Parameters() : file_name(""), iteration_count(0),
    endless_execution(false), print_after_each_step(false) {}
    
    string file_name;
    uint32_t iteration_count;
    bool endless_execution;
    bool print_after_each_step;
};


Parameters get_cl_parameters(int argc, char* argv[])
{
    Parameters p;
    bool file_flag = false;
    bool it_count_flag = false;
    
    for(int i = 0; i < argc; i++)
    {
        string arg = string(argv[i]);
        if(arg == "-h")
        {
            cout << endl << "Options:" << endl;
            cout << "\"-h\" Prints this screen" << endl;
            cout << "\"-f\" <filename> Allows to specify the input file" << endl;
            cout << "\"-e\" Enables endless execution mode" << endl;
            cout << "\"-p\" Enables tape printing after each step" << endl;
            
            exit(EXIT_SUCCESS);
        }
        
        if(arg == "-f")
        {
            file_flag = true;
            if(i + 1 < argc)
                p.file_name = string(argv[i + 1]);
            else
            {
                cout << "No file name provided after the -f flag" << endl;
                exit(EXIT_FAILURE);
            }
        }
        
        if(arg == "-n")
        {
            if(i + 1 < argc)
            {
                it_count_flag = true;
                p.iteration_count = atoi(argv[i + 1]);
            }
            else
            {
                cout << "No integer for iteration count provided!!!" << endl;
                exit(EXIT_FAILURE);
            }
        }
        
        if(arg == "-e")
        {
            p.endless_execution = true;
        }
        
        if(arg == "-p")
        {
            p.print_after_each_step = true;
        }
    }
    
    if(!file_flag)
    {
        cout << "No filename flag found, make sure it is provided." << endl;
        exit(EXIT_FAILURE);
    }
    
    if(!it_count_flag && !p.endless_execution)
    {
        cout << "No iteration flag found, and no endless execution flag found make sure either one is provided." << endl;
        exit(EXIT_FAILURE);
    }
    
    return p;
}

TuringMachine read_turing_file_and_create(istream& in)
{
    int current_pos = 0;
    string tape;
    
    
    in >> current_pos >> tape;
    
    TuringMachine t(current_pos, tape);
    
    Transition tr;
    
    while (in >> tr)
        t.add_state_transition(tr);
    
    return t;
}


int main(int argc, char* argv[])
{
    Parameters p = get_cl_parameters(argc, argv);
    
    string& file = p.file_name;
    
    ifstream in(file.c_str());
    
    if (!in.is_open())
    {
        cout << "Cannot open the file specified!" << endl;
        exit(EXIT_FAILURE);
    }
    
    TuringMachine st = read_turing_file_and_create(in);
    
    cout << "Initial tape was: " << endl;
    cout << st.get_tape() << endl;
    
    //Execute a number of times or endless if specified till the machine halts
    for(int i = 0; (i < p.iteration_count || p.endless_execution) && !st.has_halted();i++)
    {
        st.advance_state();
        
        if(p.print_after_each_step)
            cout << st.get_tape() << endl;
    }
    
    
    cout << "Tape after execution: " << endl;
    cout << st.get_tape() << endl;
    
    if(st.has_halted())
        cout << "Turing machine halted with state: " << st.get_current_state() << endl;
    
    exit(EXIT_SUCCESS);
}
